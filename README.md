### This is not free software.  This plugin uses the Onyx sofware development kits (SDKs) for Android and iOS.  It requires a license agreement with: 
[Telos Corporation](https://www.telos.com)

# @telosid/cordova-plugin-onyx

# Telos ID NPM Registry

```bash
echo @telosid:registry=https://gitlab.com/api/v4/projects/29462567/packages/npm/ >> .npmrc
```

## Install
```bash
npm install @telosid/cordova-plugin-onyx @telosid/onyx-typedefs
```

## [API](https://gitlab.com/telosid/plugins/onyx-typedefs#api)

## Preferences
| Preference       | Default              |
| ------------- |----------------------|
| **`ONYX_ANDROID_SDK_VERSION`** | <code>`8.4.1`</code> |
| **`ONYX_IOS_SDK_VERSION`**   | <code>`8.4.1`</code> |
