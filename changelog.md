# Version 8.4.1
* Update to Android and iOS ONYX SDK versions to 8.4.1 for TensorFlow fixes and iOS Camera
* compatibility changes


# Version 8.3.2
* Add support for triggerCaptureOnFingerprintDetectionTimeout to automatically trigger capture when FingerprintDetectionTimeout threshold is reached

# Version 8.3.1
* Updated to Android SDK to 8.3.0 for TensorFlow Lite version change to 2.9.0

# Version 8.2.2
* Fixed issue with Samsung Galaxy A31 crashes for FacePhi

# Version 8.2.1
* Fixed bug with base64EncodedFingerprintTemplate being empty on iOS

# Version 8.2.0
* Updated plugin to include AndroidX AppCompat support for cordova-android 10+
* Removed references to JCenter

# Version 8.1.1
* Updated iOS SDK to 8.2.1 for iPhone 14 zoom factor fixes

# Version 8.1.0
* Added fingerDetectionTimeout OnyxConfig option

# Version 8.0.4
* Update to allow simultaneous return of slap imagery and segmented imagery

# Version 8.0.1
* Update to allow overriding of text on capture screen to customize text for different languages

# Version 8.0.0
* Update to Onyx 8.0 to synchronize SDKs between Android and iOS
* Lots of breaking changes to method and variable names
* ONYX Cocoapod 8.x is using new Gitlab private repo

# Version 7.1.5
* Add returnFullFrameImage with a fullFrameMaxImageHeight
* Bugfix for flash off when `useFlash` is undefined
* Updated ONYX Cocoapod to 7.1.13 to fixed fingerprint output allowing matching between iOS probe and Android enrollment
* Removed `wholeFingerCrop` from IOnyxConfiguration
* Output images as PNG dataUris

# Version 7.1.4
* Bugfix for android package name

# Version 7.1.3
* Telos ID
* ONYX 4 finger capture

# Version 5.2.0
* Updated to use onyxCamera Cocoapod v5.3.0
* Changed OnyxMatch to use pyramidVerify()
* Added new fingerDetect property for OnyxConfiguration
* Removed shouldInvert
* Fixed bug with useFlash
* Added FLAG_ACTIVITY_NEW_TASK for starting OnyxActivity
* Changed default crop size and crop factor
* Removed FLIP enum
* Added IMAGE_ROTATION and FINGER_DETECT_MODE enums

# Version 5.1.1
## What's New
* Updated to use OnyxCamera Cocoapod 5.1.1
* Updated to use onyx-camera 5.1.2
* implemented `useManualCapture` on iOS
* Added capture screen customizations to match changes to OnyxCamera CocoaPod.
    * Added new configuration options
        * `backButtonText`
        * `showManualCaptureText`
        * `manualCaptureText`
        * `infoText`
        * `infoTextColorHexString`
        * `base64ImageData`
* Included ability to localize text for back button and manual capture text.
* Added ability to add custom message and image to capture screen.

# Version 5.0.2
## What's New
* Updated to onyx-camera:5.1.1
* Fixed issues with OnyxCallback
* Changed install location of `build-extras.gradle`
* Added `abiFilters` to `build-extras.gradle`
* Restart capture if `AUTOFOCUS_FAILURE` error is received.


# Version 5.0.1
## What's New
* Fixed bugs introduced by updating to onyx-camera:5.0.8 in build-extras.gradle

# Version 5.0.0
## What's New
* Updated to Onyx version 5.0
#### Breaking Changes
* Using the new OnyxCamera CocoaPod for iOS.
* Refactored `Onyx.Action` to `Onyx.ACTION`.
    * Removed actions `ENROLL`, `VERIFY`, `TEMPLATE`, and `IMAGE` and replaced with a single `CAPTURE` action.
    * Added on-device matching through use of the `MATCH` action.
* Brand new `OnyxOptions` for configuring the Onyx capture screen.
* Brand new `OnyxResult` object returned via `onyx.onSuccess` callback.



# Version 0.2.0
## What's New

* Added the ability to request the return of multiple image types
    * Changed `OnyxOptions.imageType` to `OnyxOptions.imageTypes`
        * Changed option argument from a single `Onyx.ImageType` to an `Array<Onyx.ImageType>`
    * Changed `OnyxResult.imageUri` to `OnyxResult.images`
        * `Onyx.ACTION.IMAGE` will now return a JSON Object
        ```
        {
            "raw": "base64EncodedImageUri",
            "preprocessed": "base64EncodedImageUri",
            "enhanced": "base64EncodedImageUri",
            "wsq": {
                "bytes": "base64EncodedBytes",
                "nfiqScore": number
            }
        }
        ```
    * Added `Onyx.ImageType.WSQ`
        * Option argument to request a base64 encoded WSQ image.

## Breaking Changes
 * Changed `OnyxOptions.imageType` to `OnyxOptions.imageTypes`
 * Changed `OnyxResult.imageUri` to `OnyxResult.images`
