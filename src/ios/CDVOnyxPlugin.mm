#import "CDVOnyxPlugin.h"
#import <Cordova/CDV.h>
#import <OnyxCamera/OnyxMatch.h>

@interface CDVOnyxPlugin ()

@property NSString* executeAction;
@property NSString* callbackId;
@property NSDictionary* args;

@end

@implementation CDVOnyxPlugin
NSString * const ACTION = @"action";
NSString * const PLUGIN_ACTION = ACTION;
NSString * const PLUGIN_ACTION_MATCH = @"match";
NSString * const PLUGIN_ACTION_CAPTURE = @"capture";
NSString * const LICENSE_KEY = @"licenseKey";
NSString * const RETURN_RAW_IMAGE = @"returnRawImage";
NSString * const RETURN_PROCESSED_IMAGE = @"returnProcessedImage";
NSString * const RETURN_ENHANCED_IMAGE = @"returnEnhancedImage";
NSString * const RETURN_SLAP_IMAGE = @"returnSlapImage";
NSString * const RETURN_SLAP_WSQ = @"returnSlapWSQ";
NSString * const SHOULD_BINARIZE_PROCESSED_IMAGE = @"shouldBinarizeProcessedImage";
NSString * const RETURN_FULL_FRAME_IMAGE = @"returnFullFrameImage";
NSString * const FULL_FRAME_MAX_IMAGE_HEIGHT = @"fullFrameMaxImageHeight";
NSString * const RETURN_WSQ = @"returnWSQ";
NSString * const RETURN_FINGERPRINT_TEMPLATE = @"returnFingerprintTemplate";
NSString * const CROP_SIZE = @"cropSize";
NSString * const CROP_SIZE_WIDTH = @"width";
NSString * const CROP_SIZE_HEIGHT = @"height";
NSString * const CROP_FACTOR = @"cropFactor";
NSString * const SHOW_LOADING_SPINNER = @"showLoadingSpinner";
NSString * const USE_MANUAL_CAPTURE = @"useManualCapture";
NSString * const MANUAL_CAPTURE_TEXT = @"manualCaptureText";
NSString * const CAPTURE_FINGERS_TEXT = @"captureFingersText";
NSString * const CAPTURE_THUMB_TEXT = @"captureThumbText";
NSString * const FINGERS_NOT_IN_FOCUS_TEXT = @"fingersNotInFocusText";
NSString * const THUMB_NOT_IN_FOCUS_TEXT = @"thumbNotInFocusText";
NSString * const USE_ONYX_LIVE = @"useOnyxLive";
NSString * const USE_FLASH = @"useFlash";
NSString * const RETICLE_ORIENTATION = @"reticleOrientation";
NSString * const COMPUTE_NFIQ_METRICS = @"computeNfiqMetrics";
NSString * const TARGET_PIXELS_PER_INCH = @"targetPixelsPerInch";
NSString * const SUBJECT_ID = @"subjectId";
NSString * const UPLOAD_METRICS = @"uploadMetrics";
NSString * const RETURN_ONYX_ERROR_ON_LOW_QUALITY = @"returnOnyxErrorOnLowQuality";
NSString * const CAPTURE_QUALITY_THRESHOLD = @"captureQualityThreshold";
NSString * const FINGER_DETECTION_TIMEOUT = @"fingerDetectionTimeout";
NSString * const TRIGGER_CAPTURE_ON_FINGER_DETECTION_TIMEOUT = @"triggerCaptureOnFingerDetectionTimeout";
NSString * const RETICLE_ORIENTATION_LEFT = @"LEFT";
NSString * const RETICLE_ORIENTATION_RIGHT = @"RIGHT";
NSString * const RETICLE_ORIENTATION_THUMB_PORTRAIT = @"THUMB_PORTRAIT";
NSString * const FINGERPRINT_TEMPLATE_TYPE_NONE = @"NONE";
NSString * const FINGERPRINT_TEMPLATE_TYPE_INNOVATRICS = @"INNOVATRICS";
NSString * const FINGERPRINT_TEMPLATE_TYPE_ISO = @"ISO";
NSString * const PROBE = @"probe";
NSString * const REFERENCE = @"reference";
NSString * const PYRAMID_SCALES = @"pyramidScales";

NSString * const PROCESSED_FINGERPRINT_DATA_URI = @"processedFingerprintDataUri";
NSString * const ENHANCED_FINGERPRINT_DATA_URI = @"enhancedFingerprintDataUri";
NSString * const BASE64_ENCODED_SLAP_WSQ_BYTES = @"base64EncodedSlapWsqBytes";
NSString * const BASE64_ENCODED_WSQ_BYTES = @"base64EncodedWsqBytes";
NSString * const BASE64_ENCODED_FINGERPRINT_TEMPLATE = @"base64EncodedFingerprintTemplate";
NSString * const CAPTURE_METRICS = @"captureMetrics";
NSString * const LIVENESS_CONFIDENCE = @"livenessConfidence";
NSString * const QUALITY_METRIC = @"qualityMetric";
NSString * const NFIQ_METRICS = @"nfiqMetrics";
NSString * const ONYX_RESULTS = @"onyxResults";
NSString * const NFIQ_SCORE = @"nfiqScore";
NSString * const RAW_FINGERPRINT_DATA_URI = @"rawFingerprintDataUri";
NSString * const SLAP_IMAGE_DATA_URI = @"slapImageDataUri";
NSString * const FULL_FRAME_IMAGE_DATA_URI = @"fullFrameImageDataUri";
NSString * const CAPTURE_FROM_FINGER_DETECTION_TIMEOUT = @"captureFromFingerDetectionTimeout";
NSString * const ONYX_CONFIGURATION = @"onyxConfiguration";



- (void)match:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* pluginResult = nil;
    _callbackId = command.callbackId;
    _args = [command.arguments objectAtIndex:0];
    _executeAction = [_args objectForKey:PLUGIN_ACTION];
    NSLog(@"action: %@", _executeAction);

    if (_args != nil && [_executeAction length] > 0 && [_executeAction isEqualToString:PLUGIN_ACTION_MATCH]) {
        NSString* probeString = [_args objectForKey:PROBE];
        NSString* referenceString = [_args objectForKey:REFERENCE];
        NSArray* scalesArray = [_args objectForKey:PYRAMID_SCALES];
        if (nil != PROBE && ![probeString isEqualToString:@""] && nil != referenceString && ![referenceString isEqualToString:@""]) {
            NSData* referenceData = [[NSData alloc] initWithBase64EncodedString:referenceString options:0];
            NSString* probeEncodedDataString = [probeString substringFromIndex:[IMAGE_URI_PREFIX length]];
            NSData* probeData = [[NSData alloc] initWithBase64EncodedString:probeEncodedDataString options:0];
            UIImage* probeImage = [UIImage imageWithData:probeData];
            double score = [OnyxMatch pyramidMatch:referenceData withImage:probeImage scales:scalesArray];
            float threshold = 0.03f;
            Boolean isVerified = score > threshold;
            NSArray* keysArray = [NSArray arrayWithObjects:ACTION, @"isVerified", @"matchScore", nil];
            NSArray*valuesArray = [NSArray arrayWithObjects:_executeAction, [NSNumber numberWithBool:isVerified], [NSNumber numberWithFloat:score], nil];
            NSMutableDictionary* resultJSON = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultJSON];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];
            return;
        }
    }

    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];
}

- (void)capture:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* pluginResult = nil;
    _callbackId = command.callbackId;
    _args = [command.arguments objectAtIndex:0];
    _executeAction = [_args objectForKey:PLUGIN_ACTION];
    NSLog(@"action: %@", _executeAction);

    if (_args != nil && [_executeAction length] > 0 && [_executeAction isEqualToString:PLUGIN_ACTION_CAPTURE]) {
        [self setupOnyx];
        return;
    }
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:_callbackId];

}

// private method
- (void)setupOnyx {
    NSString* onyxLicense = [_args objectForKey:LICENSE_KEY];

    OnyxConfigurationBuilder* onyxConfigBuilder = [[OnyxConfigurationBuilder alloc] init];
    onyxConfigBuilder
        .licenseKey(onyxLicense)
        .successCallback([self onyxSuccessCallback])
        .errorCallback([self onyxErrorCallback])
        .onyxCallback([self onyxCallback]);

    if ([[_args objectForKey:RETURN_RAW_IMAGE] boolValue]) {
        onyxConfigBuilder.returnRawImage([[_args objectForKey:RETURN_RAW_IMAGE] boolValue]);
    }

    if ([[_args objectForKey:RETURN_PROCESSED_IMAGE] boolValue]) {
        onyxConfigBuilder.returnProcessedImage([[_args objectForKey:RETURN_PROCESSED_IMAGE] boolValue]);
    }

    if ([[_args objectForKey:RETURN_ENHANCED_IMAGE] boolValue]) {
        onyxConfigBuilder.returnEnhancedImage([[_args objectForKey:RETURN_ENHANCED_IMAGE] boolValue]);
    }

    if ([[_args objectForKey:RETURN_SLAP_IMAGE] boolValue]) {
        onyxConfigBuilder.returnSlapImage([[_args objectForKey:RETURN_SLAP_IMAGE] boolValue]);
    }

    if ([[_args objectForKey:RETURN_SLAP_WSQ] boolValue]) {
        onyxConfigBuilder.returnSlapWsqData([[_args objectForKey:RETURN_SLAP_WSQ] boolValue]);
    }

    if ([[_args objectForKey:SHOULD_BINARIZE_PROCESSED_IMAGE] boolValue]) {
        onyxConfigBuilder.shouldBinarizeProcessedImage([[_args objectForKey:SHOULD_BINARIZE_PROCESSED_IMAGE] boolValue]);
    }

    if ([[_args objectForKey:RETURN_FULL_FRAME_IMAGE] boolValue]) {
        onyxConfigBuilder.returnFullFrameImage([[_args objectForKey:RETURN_FULL_FRAME_IMAGE] boolValue]);
    }

    if ([_args objectForKey:FULL_FRAME_MAX_IMAGE_HEIGHT]) {
        onyxConfigBuilder.fullFrameMaxImageHeight([[_args objectForKey:FULL_FRAME_MAX_IMAGE_HEIGHT] floatValue]);
    }

    if ([[_args objectForKey:RETURN_WSQ] boolValue]) {
        onyxConfigBuilder.returnWSQ([[_args objectForKey:RETURN_WSQ] boolValue]);
    }

    if ([_args objectForKey:RETURN_FINGERPRINT_TEMPLATE]) {
        NSString* fingerprintTemplateTypeString = [_args objectForKey:RETURN_FINGERPRINT_TEMPLATE];
        if (![fingerprintTemplateTypeString isEqualToString:@""]) {
            FingerprintTemplateType fingerprintTemplateType = NONE;
            if ([fingerprintTemplateTypeString isEqualToString:FINGERPRINT_TEMPLATE_TYPE_INNOVATRICS]) {
                fingerprintTemplateType = INNOVATRICS;
            } else if ([fingerprintTemplateTypeString isEqualToString:FINGERPRINT_TEMPLATE_TYPE_ISO]) {
                fingerprintTemplateType = ISO;
            }
            onyxConfigBuilder.returnFingerprintTemplate(fingerprintTemplateType);
        }
    }

    if ([_args objectForKey:CROP_SIZE]) {
        NSDictionary* cropSize = [_args objectForKey:CROP_SIZE];
        float width = 512;
        float height = 300;
        float floatValue = 0;
        floatValue = [[cropSize objectForKey:CROP_SIZE_WIDTH] floatValue];
        if (floatValue != 0) {
            width = floatValue;
        }
        floatValue = 0;
        floatValue = [[cropSize objectForKey:CROP_SIZE_HEIGHT] floatValue];
        if (floatValue != 0) {
            height = floatValue;
        }
        onyxConfigBuilder.cropSize(CGSizeMake(width, height));
    }

    if ([_args objectForKey:CROP_FACTOR]) {
        onyxConfigBuilder.cropFactor([[_args objectForKey:CROP_FACTOR] floatValue]);
    }

    if ([[_args objectForKey:SHOW_LOADING_SPINNER] boolValue]) {
        onyxConfigBuilder.showLoadingSpinner([[_args objectForKey:SHOW_LOADING_SPINNER] boolValue]);
    }

    if ([[_args objectForKey:USE_MANUAL_CAPTURE] boolValue]) {
        onyxConfigBuilder.useManualCapture([[_args objectForKey:USE_MANUAL_CAPTURE] boolValue]);
    }

    if ([_args objectForKey:MANUAL_CAPTURE_TEXT]) {
        NSString *manualCaptureText = [_args objectForKey:MANUAL_CAPTURE_TEXT];
        if (![manualCaptureText isEqualToString:@""]) {
            onyxConfigBuilder.manualCaptureText(manualCaptureText);
        }
    }

    if ([_args objectForKey:CAPTURE_FINGERS_TEXT]
        && [_args objectForKey:CAPTURE_THUMB_TEXT]
        && [_args objectForKey:FINGERS_NOT_IN_FOCUS_TEXT]
        && [_args objectForKey:THUMB_NOT_IN_FOCUS_TEXT]) {

        if ([_args objectForKey:CAPTURE_FINGERS_TEXT]) {
            NSString *captureFingersText = [_args objectForKey:CAPTURE_FINGERS_TEXT];
            if (![captureFingersText isEqualToString:@""]) {
                onyxConfigBuilder.captureFingersText(captureFingersText);
            }
        }

        if ([_args objectForKey:CAPTURE_THUMB_TEXT]) {
            NSString *captureThumbText = [_args objectForKey:CAPTURE_THUMB_TEXT];
            if (![captureThumbText isEqualToString:@""]) {
                onyxConfigBuilder.captureThumbText(captureThumbText);
            }
        }

        if ([_args objectForKey:FINGERS_NOT_IN_FOCUS_TEXT]) {
            NSString *fingersNotInFocusText = [_args objectForKey:FINGERS_NOT_IN_FOCUS_TEXT];
            if (![fingersNotInFocusText isEqualToString:@""]) {
                onyxConfigBuilder.fingersNotInFocusText(fingersNotInFocusText);
            }
        }

        if ([_args objectForKey:THUMB_NOT_IN_FOCUS_TEXT]) {
            NSString *thumbNotInFocusText = [_args objectForKey:THUMB_NOT_IN_FOCUS_TEXT];
            if (![thumbNotInFocusText isEqualToString:@""]) {
                onyxConfigBuilder.thumbNotInFocusText(thumbNotInFocusText);
            }
        }
    }

    if ([[_args objectForKey:USE_ONYX_LIVE] boolValue]) {
        onyxConfigBuilder.useOnyxLive([[_args objectForKey:USE_ONYX_LIVE] boolValue]);
    }

    // Check if the key/value pair exists, meaning it was a configured option, so we won't set it so that is will use
    // the OnyxConfiguration default value of true
    if ([_args objectForKey:USE_FLASH] != nil ) {
        onyxConfigBuilder.useFlash([[_args objectForKey:USE_FLASH] boolValue]);
    }

    if ([_args objectForKey:RETICLE_ORIENTATION]) {
        NSString* reticleOrientationString = [_args objectForKey:RETICLE_ORIENTATION];
        if (![reticleOrientationString isEqualToString:@""]) {
            ReticleOrientation orientation = LEFT;
            if ([reticleOrientationString isEqualToString:RETICLE_ORIENTATION_LEFT]) {
                orientation = LEFT;
            } else if ([reticleOrientationString isEqualToString:RETICLE_ORIENTATION_RIGHT]) {
                orientation = RIGHT;
            } else if ([reticleOrientationString isEqualToString:RETICLE_ORIENTATION_THUMB_PORTRAIT]) {
                orientation = THUMB_PORTRAIT;
            }
            onyxConfigBuilder.reticleOrientation(orientation);
        }
    }

    if ([_args objectForKey:COMPUTE_NFIQ_METRICS]) {
        onyxConfigBuilder.computeNfiqMetrics([[_args objectForKey:COMPUTE_NFIQ_METRICS] boolValue]);
    }

    if ([_args objectForKey:TARGET_PIXELS_PER_INCH]) {
        onyxConfigBuilder.targetPixelsPerInch([[_args objectForKey:TARGET_PIXELS_PER_INCH] floatValue]);
    }

    if ([_args objectForKey:SUBJECT_ID]) {
        onyxConfigBuilder.subjectId([_args objectForKey:SUBJECT_ID]);
    }

    if ([_args objectForKey:UPLOAD_METRICS]) {
        onyxConfigBuilder.uploadMetrics([[_args objectForKey:UPLOAD_METRICS] boolValue]);
    }

    if ([_args objectForKey:RETURN_ONYX_ERROR_ON_LOW_QUALITY]) {
        onyxConfigBuilder.returnOnyxErrorOnLowQuality([[_args objectForKey:RETURN_ONYX_ERROR_ON_LOW_QUALITY] boolValue]);
    }

    if ([_args objectForKey:CAPTURE_QUALITY_THRESHOLD]) {
        onyxConfigBuilder.captureQualityThreshold([[_args objectForKey:CAPTURE_QUALITY_THRESHOLD] floatValue]);
    }

    if ([_args objectForKey:FINGER_DETECTION_TIMEOUT]) {
        onyxConfigBuilder.fingerDetectionTimeout([[_args objectForKey:FINGER_DETECTION_TIMEOUT] intValue]);
    }

    if([_args objectForKey:TRIGGER_CAPTURE_ON_FINGER_DETECTION_TIMEOUT]) {
        onyxConfigBuilder.triggerCaptureOnFingerDetectionTimeout(
                                                                 [[_args objectForKey:TRIGGER_CAPTURE_ON_FINGER_DETECTION_TIMEOUT] boolValue]);
    }

    [onyxConfigBuilder buildOnyxConfiguration];
}

- (void(^)(Onyx* configuredOnyx))onyxCallback {
    return ^(Onyx* configuredOnyx) {
        NSLog(@"Onyx Callback");
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            [configuredOnyx capture:self.viewController];
        }];
    };
}

- (void(^)(OnyxResult* onyxResult))onyxSuccessCallback {
    return ^(OnyxResult* onyxResult) {
        NSLog(@"Onyx Success Callback");
        self->_onyxResult = onyxResult;

        NSMutableArray* onyxResults = [[NSMutableArray alloc] init];
        NSMutableArray* rawFingerprintImages = nil;
        NSMutableArray* processedFingerprintImages = nil;
        NSMutableArray* enhancedFingerprintImages = nil;
        NSMutableArray* wsqDataArray = nil;
        NSMutableArray* fingerprintTemplates = nil;
        NSUInteger numberFingersProcessed = 0;

        UIImage* slapImage = onyxResult.slapImage;
        NSData* slapWsqData = onyxResult.slapWsqData;
        UIImage* fullFrameImage = onyxResult.fullFrameImage;
        OnyxConfiguration* onyxConfiguration = onyxResult.onyxConfiguration;

        if (nil != [self->_onyxResult getRawFingerprintImages]) {
            rawFingerprintImages = [self->_onyxResult getRawFingerprintImages];
            if ([rawFingerprintImages count] > numberFingersProcessed) {
                numberFingersProcessed = [rawFingerprintImages count];
            }
        }
        if (nil != [self->_onyxResult getProcessedFingerprintImages]) {
            processedFingerprintImages = [self->_onyxResult getProcessedFingerprintImages];
            if ([processedFingerprintImages count] > numberFingersProcessed) {
                numberFingersProcessed = [processedFingerprintImages count];
            }
        }
        if (nil != [self->_onyxResult getEnhancedFingerprintImages]) {
            enhancedFingerprintImages = [self->_onyxResult getEnhancedFingerprintImages];
            if ([enhancedFingerprintImages count] > numberFingersProcessed) {
                numberFingersProcessed = [enhancedFingerprintImages count];
            }
        }
        if (nil != [self->_onyxResult getWsqData]) {
            wsqDataArray = [self->_onyxResult getWsqData];
            if ([wsqDataArray count] > numberFingersProcessed) {
                numberFingersProcessed = [wsqDataArray count];
            }
        }
        if (nil != [self->_onyxResult getFingerprintTemplates]) {
            fingerprintTemplates = [self->_onyxResult getFingerprintTemplates];
            if ([fingerprintTemplates count] > numberFingersProcessed) {
                numberFingersProcessed = [fingerprintTemplates count];
            }
        }

        NSArray* keysArray;
        NSArray* valuesArray;
        NSMutableDictionary* iOnyxResult;

        for (int i = 0; i < numberFingersProcessed; i++) {
            NSString* rawImageUri = @"";
            NSString* processedImageUri = @"";
            NSString* enhancedImageUri = @"";
            NSString* base64EncodedWsq = @"";
            NSString* base64EncodedFingerprintTemplate = @"";
            NSMutableDictionary* captureMetricsJson;

            if ([[self->_args objectForKey:RETURN_RAW_IMAGE] boolValue]) {
                if (nil != rawFingerprintImages && [rawFingerprintImages count] == numberFingersProcessed) {
                    rawImageUri = [self getFingerprintImageUri:rawFingerprintImages[i]];
                }
            }
            if ([[self->_args objectForKey:RETURN_PROCESSED_IMAGE] boolValue]) {
                if (nil != processedFingerprintImages && [processedFingerprintImages count] == numberFingersProcessed) {
                    processedImageUri = [self getFingerprintImageUri:processedFingerprintImages[i]];
                }
            }
            if ([[self->_args objectForKey:RETURN_ENHANCED_IMAGE] boolValue]) {
                if (nil != enhancedFingerprintImages && [enhancedFingerprintImages count] == numberFingersProcessed) {
                    enhancedImageUri = [self getFingerprintImageUri:enhancedFingerprintImages[i]];
                }
            }
            if ([[self->_args objectForKey:RETURN_WSQ] boolValue]) {
                if (nil != wsqDataArray && [wsqDataArray count] == numberFingersProcessed) {
                    base64EncodedWsq = [self getBase64EncodedString:wsqDataArray[i]];
                }
            }
            if (![[self->_args objectForKey:RETURN_FINGERPRINT_TEMPLATE] containsString:FINGERPRINT_TEMPLATE_TYPE_NONE]) {
                if (nil != fingerprintTemplates && [fingerprintTemplates count] == numberFingersProcessed) {
                    base64EncodedFingerprintTemplate = [self getBase64EncodedString:fingerprintTemplates[i]];
                }
            }

            if (nil != [self->_onyxResult getMetrics]) {
                NSMutableDictionary* nfiqMetricsJson = [[NSMutableDictionary alloc] init];
                CaptureMetrics* metrics = [self->_onyxResult getMetrics];
                int nfiqScore = 0;
                float qualityMetric = [metrics getQualityMetric];
                float livenessConfidence = [metrics getLivenessConfidence];

                if (nil != [metrics getNfiqMetrics]) {
                    NSMutableArray* nfiqMetricsArray = [metrics getNfiqMetrics];
                    if ([nfiqMetricsArray count] == numberFingersProcessed) {
                        NfiqMetrics* nfiqMetrics = nfiqMetricsArray[i];
                        nfiqScore = [nfiqMetrics getNfiqScore];

                        keysArray = [NSArray arrayWithObjects:NFIQ_SCORE, nil];
                        valuesArray = [NSArray arrayWithObjects: [NSNumber numberWithInteger: nfiqScore], nil];
                        nfiqMetricsJson = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
                    }
                }

                keysArray = [NSArray arrayWithObjects:LIVENESS_CONFIDENCE, QUALITY_METRIC, NFIQ_METRICS, nil];
                valuesArray = [NSArray arrayWithObjects: [NSNumber numberWithFloat: livenessConfidence], [NSNumber numberWithFloat: qualityMetric], nfiqMetricsJson, nil];
                captureMetricsJson = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
            }
            keysArray = [NSArray arrayWithObjects:RAW_FINGERPRINT_DATA_URI, PROCESSED_FINGERPRINT_DATA_URI,
                         ENHANCED_FINGERPRINT_DATA_URI, BASE64_ENCODED_WSQ_BYTES,
                         BASE64_ENCODED_FINGERPRINT_TEMPLATE, CAPTURE_METRICS, nil];
            valuesArray = [NSArray arrayWithObjects:rawImageUri, processedImageUri, enhancedImageUri, base64EncodedWsq, base64EncodedFingerprintTemplate, captureMetricsJson, nil];
            iOnyxResult = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
            onyxResults[i] = iOnyxResult;
        }

        if (nil != slapImage) {
            NSString* slapImageDataUri = [self getFingerprintImageUri:slapImage];
            NSArray* keysArray = [NSArray arrayWithObject:SLAP_IMAGE_DATA_URI];
            NSArray* valuesArray = [NSArray arrayWithObject:slapImageDataUri];
            if (onyxResults.count != 0) {
                NSMutableDictionary* existingOnyxResult = onyxResults[0];
                existingOnyxResult[SLAP_IMAGE_DATA_URI] = slapImageDataUri;
            } else {
                onyxResults[0] = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
            }
        }

        if (nil != slapWsqData) {
            NSString* base64EncodedSlapWsq = [self getBase64EncodedString:slapWsqData];
            NSArray* keysArray = [NSArray arrayWithObject:BASE64_ENCODED_SLAP_WSQ_BYTES];
            NSArray* valuesArray = [NSArray arrayWithObject:base64EncodedSlapWsq];
            if (onyxResults.count != 0) {
                NSMutableDictionary* existingOnyxResult = onyxResults[0];
                existingOnyxResult[BASE64_ENCODED_SLAP_WSQ_BYTES] = base64EncodedSlapWsq;
            } else {
                onyxResults[0] = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
            }
        }

        if (nil != fullFrameImage) {
            NSString* fullFrameImageDataUri = [self getFingerprintImageUri:fullFrameImage];
            NSArray* keysArray = [NSArray arrayWithObject:FULL_FRAME_IMAGE_DATA_URI];
            NSArray* valuesArray = [NSArray arrayWithObject:fullFrameImageDataUri];
            if (onyxResults.count != 0) {
                NSMutableDictionary* existingOnyxResult = onyxResults[0];
                existingOnyxResult[FULL_FRAME_IMAGE_DATA_URI] = fullFrameImageDataUri;
            } else {
                onyxResults[0] = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
            }
        }

        bool isCaptureFromFingerDetectionTimeout = onyxResult.isCaptureFromFingerDetectionTimeout;
        if (onyxResults.count != 0) {
            NSMutableDictionary* existingOnyxResult = onyxResults[0];
            if (isCaptureFromFingerDetectionTimeout) {
                existingOnyxResult[CAPTURE_FROM_FINGER_DETECTION_TIMEOUT] = @YES;
            } else {
                existingOnyxResult[CAPTURE_FROM_FINGER_DETECTION_TIMEOUT] = @NO;
            }
        } else {
            NSArray* keysArray = [NSArray arrayWithObject:CAPTURE_FROM_FINGER_DETECTION_TIMEOUT];
            NSArray* valuesArray;
            if (isCaptureFromFingerDetectionTimeout) {
                valuesArray = [NSArray arrayWithObject:@YES];
            } else {
                valuesArray = [NSArray arrayWithObject:@NO];
            }
            NSMutableDictionary* captureFromFingerDetectionTimeoutDict = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
            onyxResults[0] = captureFromFingerDetectionTimeoutDict;
        }

        if (nil != onyxConfiguration) {
            FingerprintTemplateType fingerprintTemplateType = onyxConfiguration.returnFingerprintTemplate;
            NSString* fingerprintTemplateTypeString = @"NONE";
            if (fingerprintTemplateType == INNOVATRICS) {
                fingerprintTemplateTypeString = @"INNOVATRICS";
            } else if (fingerprintTemplateType == ISO) {
                fingerprintTemplateTypeString = @"ISO";
            }
            NSArray* keysArrayForOnyxConfiguration = [NSArray arrayWithObject:RETURN_FINGERPRINT_TEMPLATE];
            NSArray* valuesArrayForOnyxConfiguration = [NSArray arrayWithObject:fingerprintTemplateTypeString];
            NSMutableDictionary* returnFingerprintTemplateDictionary = [NSMutableDictionary dictionaryWithObjects:valuesArrayForOnyxConfiguration forKeys:keysArrayForOnyxConfiguration];

            if (onyxResults.count != 0) {
                NSMutableDictionary* existingOnyxResult = onyxResults[0];
                existingOnyxResult[ONYX_CONFIGURATION] = returnFingerprintTemplateDictionary;
            } else {
                NSArray* keysArray = [NSArray arrayWithObject:ONYX_CONFIGURATION];
                NSArray* valuesArray = [NSArray arrayWithObject:returnFingerprintTemplateDictionary];
                NSMutableDictionary* onyxConfigDict = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
                onyxResults[0] = onyxConfigDict;
            }
        }


        keysArray = [NSArray arrayWithObjects:ACTION, ONYX_RESULTS, nil];
        valuesArray = [NSArray arrayWithObjects:self->_executeAction, onyxResults, nil];
        NSMutableDictionary* resultJSON = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];

        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultJSON];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:self->_callbackId];
    };
}

- (void(^)(OnyxError* onyxError)) onyxErrorCallback {
    return ^(OnyxError* onyxError) {
        NSLog(@"Onyx Error Callback");
        // Set response keys
        NSArray* keysArray = [NSArray arrayWithObjects: @"error", @"message", nil];
        // Set response values
        NSArray* valuesArray = [NSArray arrayWithObjects: @(onyxError.error), onyxError.errorMessage, nil];

        // Create response object
        NSMutableDictionary* resultJSON = [NSMutableDictionary dictionaryWithObjects:valuesArray forKeys:keysArray];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:resultJSON];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:self->_callbackId];
    };
}

- (NSString*) getFingerprintImageUri:(UIImage*)fingerprintImage {
    NSString* imageUriPrefix = @"data:image/png;base64,%@";
    //    NSString* imageUriPrefix = [IMAGE_URI_PREFIX stringByAppendingString:@"%@"]; //@"data:image/jpeg;base64,%@";
    //    NSData* imageData = UIImageJPEGRepresentation(fingerprintImage, 1.0);
    NSData* imageData = UIImagePNGRepresentation(fingerprintImage);
    return [NSString stringWithFormat:imageUriPrefix, [imageData base64EncodedStringWithOptions:0]];
}

- (NSString*) getBase64EncodedString:(NSData*)data {
    return [data base64EncodedStringWithOptions:0];
}

@end
