package com.telosid.onyx.plugin.cordova;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import com.dft.onyx.FingerprintTemplate;
import com.dft.onyxcamera.util.FileUtil;

import java.io.IOException;

public class OnyxPlugin extends CordovaPlugin implements OnyxMatch.MatchResultCallback {

    public static final String TAG = "OnyxPlugin";
    public static final String IMAGE_URI_PREFIX = "data:image/png;base64,";
    public static String mPackageName;
    public static JSONObject mArgs;
    private static String mExecuteAction;
    public static OnyxPluginAction mPluginAction;

    public enum OnyxPluginAction {
        CAPTURE("capture"),
        MATCH("match"),
        REQUEST_FILE_PERMISSIONS("requestFilePermissions"),
        WRITE_FILE("writeFile");

        private final String key;

        OnyxPluginAction(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }
    }

    public enum OnyxConfig {
        ACTION("action"),
        LICENSE_KEY("licenseKey"),
        RETURN_RAW_IMAGE("returnRawImage"),
        RETURN_PROCESSED_IMAGE("returnProcessedImage"),
        RETURN_ENHANCED_IMAGE("returnEnhancedImage"),
        RETURN_SLAP_IMAGE("returnSlapImage"),
        RETURN_SLAP_WSQ("returnSlapWSQ"),
        SHOULD_BINARIZE_PROCESSED_IMAGE("shouldBinarizeProcessedImage"),
        RETURN_FULL_FRAME_IMAGE("returnFullFrameImage"),
        FULL_FRAME_MAX_IMAGE_HEIGHT("fullFrameMaxImageHeight"),
        RETURN_WSQ("returnWSQ"),
        RETURN_FINGERPRINT_TEMPLATE("returnFingerprintTemplate"),
        CROP_SIZE("cropSize"),
        CROP_SIZE_WIDTH("width"),
        CROP_SIZE_HEIGHT("height"),
        CROP_FACTOR("cropFactor"),
        SHOW_LOADING_SPINNER("showLoadingSpinner"),
        USE_MANUAL_CAPTURE("useManualCapture"),
        MANUAL_CAPTURE_TEXT("manualCaptureText"),
        CAPTURE_FINGERS_TEXT("captureFingersText"),
        CAPTURE_THUMB_TEXT("captureThumbText"),
        FINGERS_NOT_IN_FOCUS_TEXT("fingersNotInFocusText"),
        THUMB_NOT_IN_FOCUS_TEXT("thumbNotInFocusText"),
        USE_ONYX_LIVE("useOnyxLive"),
        USE_FLASH("useFlash"),
        RETICLE_ORIENTATION("reticleOrientation"),
        COMPUTE_NFIQ_METRICS("computeNfiqMetrics"),
        TARGET_PIXELS_PER_INCH("targetPixelsPerInch"),
        SUBJECT_ID("subjectId"),
        UPLOAD_METRICS("uploadMetrics"),
        RETURN_ONYX_ERROR_ON_LOW_QUALITY("returnOnyxErrorOnLowQuality"),
        CAPTURE_QUALITY_THRESHOLD("captureQualityThreshold"),
        FINGER_DETECTION_TIMEOUT("fingerDetectionTimeout"),
        TRIGGER_CAPTURE_ON_FINGER_DETECTION_TIMEOUT("triggerCaptureOnFingerDetectionTimeout"),
        RETICLE_ORIENTATION_LEFT("LEFT"),
        RETICLE_ORIENTATION_RIGHT("RIGHT"),
        RETICLE_ORIENTATION_THUMB_PORTRAIT("THUMB_PORTRAIT"),
        FINGERPRINT_TEMPLATE_TYPE_NONE("NONE"),
        FINGERPRINT_TEMPLATE_TYPE_INNOVATRICS("INNOVATRICS"),
        FINGERPRINT_TEMPLATE_TYPE_ISO("ISO"),
        REFERENCE("reference"),
        PROBE("probe"),
        PYRAMID_SCALES("pyramidScales"),
        IMAGE_BYTES("imageBytes"),
        FILE_DISPLAY_NAME("fileDisplayName");
        private final String key;

        OnyxConfig(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }
    }

    public static CallbackContext mCallbackContext;
    public static PluginResult mPluginResult;
    private Activity mActivity;
    private Context mContext;

    /**
     * Constructor
     */
    public OnyxPlugin() {
    }

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Log.v(TAG, "Init Onyx");
        mPackageName = cordova.getActivity().getApplicationContext().getPackageName();
        mPluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
        mActivity = cordova.getActivity();
        mContext = cordova.getActivity().getApplicationContext();
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action          The action to execute.
     * @param args            JSONArry of arguments for the plugin.
     * @param callbackContext The callback id used when calling back into JavaScript.
     * @return A PluginResult object with a status and message.
     */
    public boolean execute(final String action,
                           JSONArray args,
                           CallbackContext callbackContext) throws JSONException {
        mCallbackContext = callbackContext;
        Log.v(TAG, "OnyxPlugin action: " + action);
        mExecuteAction = action;

        mArgs = args.getJSONObject(0);
        if (!mArgs.has(OnyxConfig.LICENSE_KEY.getKey()) || !mArgs.has(OnyxConfig.ACTION.getKey())) {
            onError( "Missing license key or ACTION.");
            return true;
        }

        if (action.equalsIgnoreCase(OnyxPluginAction.MATCH.getKey())) {
            mPluginAction = OnyxPluginAction.MATCH;
        } else if (action.equalsIgnoreCase(OnyxPluginAction.CAPTURE.getKey())) {
            mPluginAction = OnyxPluginAction.CAPTURE;
        } else if (action.equalsIgnoreCase(OnyxPluginAction.REQUEST_FILE_PERMISSIONS.getKey())) {
            mPluginAction = OnyxPluginAction.REQUEST_FILE_PERMISSIONS;
        } else if (action.equalsIgnoreCase(OnyxPluginAction.WRITE_FILE.getKey())) {
            mPluginAction = OnyxPluginAction.WRITE_FILE;
        }

        if (null != mPluginAction) {
            switch (mPluginAction) {
                case MATCH:
                    doMatch();
                    break;
                case CAPTURE:
                    launchOnyx();
                    break;
                case REQUEST_FILE_PERMISSIONS:
                    requestFilePermissions();
                    break;
                case WRITE_FILE:
                    writeFile();
                    break;
            }
        } else {
            onError("Invalid plugin action.");
        }
        return true;
    }

    public static void onFinished(int resultCode, JSONObject result) {
        if (resultCode == Activity.RESULT_OK) {
            mPluginResult = new PluginResult(PluginResult.Status.OK);
            try {
                result.put(OnyxConfig.ACTION.getKey(), mExecuteAction);
            } catch (JSONException e) {
                String errorMessage = "Failed to set JSON key value pair: " + e.getMessage();
                mCallbackContext.error(errorMessage);
                mPluginResult = new PluginResult(PluginResult.Status.ERROR);
            }
            mCallbackContext.success(result);
        } else if (resultCode == Activity.RESULT_CANCELED) {
            mPluginResult = new PluginResult(PluginResult.Status.ERROR);
            mCallbackContext.error("Cancelled");
        }

        mCallbackContext.sendPluginResult(mPluginResult);
    }

    private void keepCordovaCallback() {
        mPluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
        mPluginResult.setKeepCallback(true);
        mCallbackContext.sendPluginResult(mPluginResult);
    }

    public static void onError(String errorMessage) {
        Log.e(TAG, errorMessage);
        mCallbackContext.error(errorMessage);
        mPluginResult = new PluginResult(PluginResult.Status.ERROR);
        mCallbackContext.sendPluginResult(mPluginResult);
    }

    private void launchOnyx() {
        mActivity.runOnUiThread(() -> {
            Intent onyxIntent = new Intent(mContext, OnyxActivity.class);
            onyxIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(onyxIntent);
        });
        keepCordovaCallback();
    }

    private void requestFilePermissions() {
        FileUtil fileUtil = new FileUtil();
        fileUtil.getWriteExternalStoragePermission(mActivity);
    }

    private void writeFile() {
        if (!mArgs.has(OnyxConfig.IMAGE_BYTES.getKey()) || !mArgs.has(OnyxConfig.FILE_DISPLAY_NAME.getKey())) {
            onError("Missing IMAGE_BYTES or FILE_DISPLAY_NAME.");
            return;
        }
        FileUtil fileUtil = new FileUtil();
        try {
            String imageBytes = mArgs.getString(OnyxConfig.IMAGE_BYTES.getKey());
            String displayName = mArgs.getString(OnyxConfig.FILE_DISPLAY_NAME.getKey());
            fileUtil.saveImage(mContext, getBitmapFromBase64EncodedString(imageBytes),
                    null, displayName);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "Exception saving image.");
            onError(e.getMessage());
        }
        mPluginResult = new PluginResult(PluginResult.Status.OK);
        mPluginResult.setKeepCallback(true);
        mCallbackContext.sendPluginResult(mPluginResult);
    }

    private void doMatch() throws JSONException {
        // Get values for JSON keys
        String encodedReference = mArgs.getString(OnyxConfig.REFERENCE.getKey());
        String encodedProbe = mArgs.getString(OnyxConfig.PROBE.getKey());
        JSONArray scalesJSONArray = null;
        if (mArgs.has(OnyxConfig.PYRAMID_SCALES.getKey())) {
            scalesJSONArray = mArgs.getJSONArray(OnyxConfig.PYRAMID_SCALES.getKey());
        }

        // Decode reference fingerprint template data
        byte[] referenceBytes = Base64.decode(encodedReference, Base64.NO_WRAP);

        Bitmap probeBitmap = getBitmapFromBase64EncodedString(encodedProbe);

        // Create a mat from the bitmap
        Mat matProbe = new Mat();
        Utils.bitmapToMat(probeBitmap, matProbe);
        Imgproc.cvtColor(matProbe, matProbe, Imgproc.COLOR_RGB2GRAY);

        // Create reference fingerprint template from bytes
        FingerprintTemplate ftRef = new FingerprintTemplate(referenceBytes, 0);

        // Convert pyramid scales from JSON array to double array
        double[] argsScales = null;
        if (null != scalesJSONArray && scalesJSONArray.length() > 0) {
            argsScales = new double[scalesJSONArray.length()];
            for (int i = 0; i < argsScales.length; i++) {
                argsScales[i] = Double.parseDouble(scalesJSONArray.optString(i));
            }
        }
        final double[] pyramidScales = argsScales;

        OnyxMatch matchTask = new OnyxMatch(mContext, OnyxPlugin.this);
        matchTask.execute(ftRef, matProbe, pyramidScales);
    }

    public static Bitmap getBitmapFromBase64EncodedString(String encodedProbe) {
        // Get encoded probe processed fingerprint image data from image URI
        String encodedProbeDataString = encodedProbe.substring(IMAGE_URI_PREFIX.length(), encodedProbe.length());

        // Decode probe probe image data
        byte[] probeBytes = Base64.decode(encodedProbeDataString, Base64.NO_WRAP);

        // Create a bitmap from the probe bytes
        Bitmap probeBitmap = BitmapFactory.decodeByteArray(probeBytes, 0, probeBytes.length);
        return probeBitmap;
    }

    @Override
    public void onMatchFinished(boolean match, float score) {
        JSONObject result = new JSONObject();
        String errorMessage = null;
        try {
            result.put("isVerified", match);
            result.put("matchScore", score);
        } catch (JSONException e) {
            errorMessage = "Failed to set JSON key value pair: " + e.toString();
        }
        if (null != errorMessage) {
            Log.e(TAG, errorMessage);
            onError(errorMessage);
        } else {
            onFinished(Activity.RESULT_OK, result);
        }
    }
}
